# Multi MNIST environment with gym like interface

This environment as it is right now is static, meaning that only a picture
is generated for each episode.

## Rewards
Rewards are given for correct classifcation.
If a prediction is made twice, but there is only one number of that value
no reward is given.

If the eos signal is given and the number of predictions matches the number of
objects in the scene a reward is given.


## Actions
Expected actions are of shape [5] and contain the following values:
location: [x, y], 1-dim vector of size 2 containing a location in pixel values
patch size: ps, scalar that represents the edge length of the image patch
prediction signal: o, scalar that determines if a prediction is made or not
based on the prediction threshold
end of sequence signal: e, scalar that determines if a eos is reached or not
based on the end of sequence threshold
prediction: p, scalar that represents the label of the predicted object

## States
Given the actions a patch of the image is returned.
