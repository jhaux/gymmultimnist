from setuptools import setup

setup(name='gym_glimpseEnv',
      version='0.0.1',
      install_requires=['numpy',
                        'torch',
                        'torchvision',
                        'matplotlib'])
