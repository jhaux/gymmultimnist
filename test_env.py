import matplotlib as mpl
mpl.use('AGG')
from gym_glimpseEnv import GlimpseEnv
import matplotlib.pyplot as plt
import numpy as np
import torch
from data_preparation import ScatterGeneratorWithVGG as ScatterGenerator
from torch.autograd import Variable
import time
from tqdm import tqdm
from glimpsenet.util.plotting import plot_activations, grid

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--bg', action='store_true', default=False,
                    help='benchmark generator')
parser.add_argument('--plt', action='store_true', default=False,
                    help='make plots of glimpses')
parser.add_argument('--em', type=str, default='nearest',
                    help='Extract method. Can be \'nearest\' or \'bilinear\' '
                         'or \'roi\'')
parser.add_argument('--n', type=int, default=5,
                    help='Number of samples per dimension')
parser.add_argument('--i', type=str, default='mm',
                    help='Image. Can be mm, face, test.')

args = parser.parse_args()


mmScatter = ScatterGenerator('mnist', 2, 1)

# nx = ny = 2
# f, AX = plt.subplots(nx, ny, figsize=[10, 10])
# AX = np.reshape(AX, [-1])
# for i, sample in enumerate(mmScatter):
#     if i > nx*ny - 1:
#         break
#     ax = AX[i]
#     ax.imshow(sample['features'][0].permute(1, 2, 0).numpy()[..., 0], 'gray')
#     targs = [str(l[0]) for l in list(sample['labels'].numpy())]
#     ax.set_title(', '.join(targs))
#     ax.axis('off')
#
# plt.show()

if args.bg:
    N_samples = 1000.
    t_0 = time.time()
    for i in range(int(N_samples)):
        sample = next(mmScatter)
        if i == N_samples:
            break
        f = sample['features']
        f.zero_()
    t_stop = time.time() - t_0
    t_dur_nc = N_samples / t_stop
    t_s_nc = t_stop / N_samples

    t_0 = time.time()
    for i in range(int(N_samples)):
        sample = next(mmScatter)
        if i == N_samples:
            break
        f = sample['features']
        f.cuda()
        f.zero_()
    t_stop = time.time() - t_0
    t_dur_c = N_samples / t_stop
    t_s_c = t_stop / N_samples

    print('no cuda: {:4.2f} samples/sec {:4.3f} sec/sample'
          .format(t_dur_nc, t_s_nc))
    print('   cuda: {:4.2f} samples/sec {:4.3f} sec/sample'
          .format(t_dur_c, t_s_c))

env = GlimpseEnv(mmScatter, extract_mode=args.em)
low_res, imsize = env.reset()
vmin = np.min(low_res.cpu().numpy())
vmax = np.max(low_res.cpu().numpy())

if args.i == 'test':
    imsize = 100, 100, 1
    image = np.linspace(0, 1, num=np.prod(imsize))
    image = np.reshape(image, imsize)

    for i in range(4):
        image[..., i*25:i*25 + 12, :] \
            = image[..., i*25:i*25 + 12, :][::-1] ** (i+1)
        image[..., i*25+12:i*25 + 25, :] \
            = image[..., i*25+12:i*25 + 25, :][::1] - 0.1*i

    image += np.min(image)
    image /= np.max(image)
    vmin = np.min(image)
    vmax = np.max(image)

    # plt.imshow(image[:, :, 0], vmin=vmin, vmax=vmax)
    # plt.show()
    image = torch.from_numpy(image).float().unsqueeze(0).cuda()
    image = image.permute(0, 3, 1, 2)
    env.image = image

low_res, imsize = env.reset()
# plt.imshow(np.squeeze(low_res[0].permute(1, 2, 0).cpu().numpy(), -1), 'gray')
# plt.show()

# im = env.image
# if im.size()[2] == 3:
#     plt.imshow(im)
# else:
#     plot_activations({'env.image': torch.squeeze(im)})
# plt.show()

N_y = N_x = args.n
N_ps = 100
ps = np.linspace(1, 120, num=N_ps)

P = []
for x in tqdm(np.linspace(-120, 120, num=N_x)):
    Py = []
    for y in tqdm(np.linspace(-120, 120, num=N_y)):
        patches = []
        for p in ps:
            loc = [x, y]
            loc = Variable(torch.cuda.FloatTensor(loc))
            p = Variable(torch.cuda.FloatTensor([p]))
            action = {'location': loc,
                      'patch_size': p,
                      'pred_signal': None,
                      'prediction': None}
            env.restart()
            obs, r, term, info = env.step(action)
            patches.append(obs)
        Py.append(patches)
    P.append(Py)

n_x = len(P)
n_y = len(P[0])

if args.plt:
    print('')
    print('Making Plots')
    for k, p in tqdm(enumerate(ps), total=N_ps):
        f, AX = plt.subplots(n_x, n_y, figsize=[12, 12])
        f.suptitle('patch size {:6.2f}'.format(p))
        for i, y in enumerate(np.linspace(-120, 120, num=N_y)):
            for j, x in enumerate(np.linspace(-120, 120, num=N_x)):
                ax = AX[i, j]
                im = P[j][i][k].cpu()
                if im.size()[1] == 3:
                    im = np.squeeze(im.squeeze(0).permute(1, 2, 0).numpy(), -1)
                else:
                    im = grid(im.squeeze().numpy())
                    vmin = vmax = None
                ax.imshow(im, vmin=vmin, vmax=vmax)
                # ax.set_title('{:2.1f}, {:2.1f}'.format(x, y))
                ax.axis('off')
                if i == 0:
                    ax.text(0.5, 1.5, '{:4.2f}'.format(x),
                            horizontalalignment='center',
                            verticalalignment='bottom',
                            transform=ax.transAxes)
                if j == 0:
                    ax.text(-0.5, 0.5, '{:4.2f}'.format(y),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=ax.transAxes)

        f.savefig('{}-{}-{}-patches.png'.format(args.i, args.em, p))
        plt.close('all')
