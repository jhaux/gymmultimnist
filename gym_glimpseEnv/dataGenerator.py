import numpy as np
import multiprocessing as mp
import torch
import os
from scipy.misc import imresize, imread


class ValidationGenerator(object):

    def __init__(self, imsize):
        self.cur_valid = 0
        self.validation_set_dir = '/home/johannes/Documents/Uni HD/' \
                                  + 'Masterarbeit/datasets/validation_sets'

        self.imsize = imsize
        self.num_objs = 1
        self.start_difficulty = self.max_difficulty = -1

    # Python 3 compatibility
    def __next__(self):
        return self.next_train()

    def __iter__(self):
        return self

    next = __next__

    def next_train(self):
        raise NotImplementedError('next has to be defined by the inheriting '
                                  'class.')

    def save_validation_set(self, size=2000, force=False):
        if not self.validation_set_exists or force:
            d = next(self)
            im = d['features']
            l = d['labels']
            n_ch = im.shape[-1]
            dtp = im.dtype
            X = np.zeros([size]+list(self.imsize)+[n_ch], dtype=dtp)
            if hasattr(self, 'l_size'):
                l_size = self.l_size
            else:
                l_size = l.shape
            Y = np.zeros([size]+list(l_size))

            i = 0
            while i < size:
                d = next(self)
                im = d['features']
                l = d['labels']
                X[i] = imresize(im, self.imsize)
                if hasattr(self, 'l_size'):
                    l_pad = -np.ones(l_size)
                    l_pad[l, :] = 1
                    l = l_pad

                Y[i] = l

                i += 1

            name_X = self.validation_set_string.format(
                    self.num_objs,
                    self.start_difficulty,
                    self.max_difficulty,
                    'X')
            name_Y = self.validation_set_string.format(
                    self.num_objs,
                    self.start_difficulty,
                    self.max_difficulty,
                    'Y')

            path_X = os.path.join(self.validation_set_dir, name_X)
            path_Y = os.path.join(self.validation_set_dir, name_Y)

            try:
                print('saving new validation sets at {}'.format(path_X))
                print('and at {}'.format(path_Y))
                np.save(path_X, X)
                np.save(path_Y, Y)
            except:
                print('Could not create validation set. This is a problem, if'
                      ' this worker is also the master')

    @property
    def validation_set_string(self):
        return 'nO{}-d{}of{}_{}_validation.npy'

    @property
    def validation_set_exists(self):
            name_X = self.validation_set_string.format(self.num_objs,
                                                       self.start_difficulty,
                                                       self.max_difficulty,
                                                       'X')
            name_Y = self.validation_set_string.format(self.num_objs,
                                                       self.start_difficulty,
                                                       self.max_difficulty,
                                                       'Y')

            self.path_X = os.path.join(self.validation_set_dir, name_X)
            self.path_Y = os.path.join(self.validation_set_dir, name_Y)

            if os.path.isfile(self.path_X) and os.path.isfile(self.path_Y):
                return True
            else:
                return False

    def next_valid(self):
        image = self.X_valid[self.cur_valid]
        label = self.Y_valid[self.cur_valid]
        if hasattr(self, 'translate_label'):
            label = self.translate_label(label)

        if image.shape[-1] == 1:
            image = image[..., 0]
        low_res = imresize(image, self.lr_shape)
        if image.shape[-1] == 1:
            low_res = np.expand_dims(low_res, -1)

        self.cur_valid += 1

        return {'features': image, 'low_res': low_res, 'labels': label}

    def reset_valid_counter(self):
        self.cur_valid = 0

    def set_mode(self, mode):
        if mode == 'test':
            self.next = self.next_valid
            self.X_valid = np.load(self.path_X, 'r')
            self.Y_valid = np.load(self.path_Y, 'r')
        else:
            self.next = self.next_train


class ScatterGenerator(ValidationGenerator):
    '''Data Generation object, that produces new image samples on the fly
    to ensure, no sample is seen twice. This should make overfitting more
    unlikely.'''

    def __init__(self,
                 dataset,
                 num_objs,
                 start_difficulty,
                 max_difficulty=6,
                 imsize=[100, 100],
                 mode='train'):
        '''
        Arguments:
            dataset: numpy array containing the base data (e.g MNIST numbers)
            numObjs: Number of objects to appear in the image
            difficulty: integer value, that detemines how the numbers are
                placed in the image
            imsize: size of the returned image. If values are 2 element lists
                a random size will be drawn within the bounds of these values.
        '''

        super(ScatterGenerator, self).__init__(imsize)
        self.num_objs = num_objs
        self.start_difficulty = start_difficulty
        self.max_difficulty = max_difficulty
        self.imsize = imsize
        self.sort_by = 'y'
        self.mode = mode
        self.num_classes = 10
        self.a_0 = 125
        self.m_0 = 0.5
        self.lr_shape = [25, 25]

        self.dset_root = dset_root = '/home/johannes/Documents/Uni HD/' \
                                     + 'Masterarbeit/datasets/'
        if dataset == 'mnist':
            self.dset_im = dset_im = 'MNIST/mnist_complete_im.npy'
            self.dset_l = dset_l = 'MNIST/mnist_complete_l.npy'

        self.images = np.load(os.path.join(dset_root, dset_im), 'r')
        self.labels = np.load(os.path.join(dset_root, dset_l), 'r')

        self.num_samples = len(self.images)
        self.patch_size = self.images[0].shape[0]

        if self.patch_size != self.images[0].shape[1]:
            raise ValueError('Images supplied to this generator need to be'
                             'square for now, but are {}x{}'
                             .format(*self.images[0].shape[0:2]))
        self.save_validation_set()
        self.set_mode(mode)

    def next_train(self):
        difficulty = self.start_difficulty  # for now
        if np.rank(self.imsize) == 2:
            [xl, xh], [yl, yh] = self.imsize
            dx = np.random.randint(low=xl, high=xh)
            dy = np.random.randint(low=yl, high=yh)
            imsize = [dx, dy]
        else:
            imsize = self.imsize
        image = np.zeros(imsize + [1])
        label = np.zeros([self.num_objs, 1])
        # image, label index -> tuple in object dimension [n_objs, im_d, l_d]
        idx_tuple = np.random.choice(self.num_samples, [self.num_objs])

        location_tuple = self.generateDistinctLocations(self.num_objs,
                                                        difficulty,
                                                        imsize)

        ps = self.patch_size
        for (x, y), idx in zip(location_tuple, idx_tuple):
            image[x:x+ps, y:y+ps] = self.images[idx]

        idx_tuple = self.sortIndexTuple(location_tuple, idx_tuple)
        label = np.vstack([self.labels[j] for j in idx_tuple])

        low_res = imresize(image[..., 0], self.lr_shape)
        low_res = np.expand_dims(low_res, -1)

        # image = torch.from_numpy(image).float().cuda()
        # low_res = torch.from_numpy(low_res).float().cuda()
        # label = torch.from_numpy(label).long().cuda()

        return {'features': image, 'low_res': low_res, 'labels': label}

    def generateDistinctLocations(self, numObjs, difficulty, imsize,
                                  possible_area=1.0):
        '''Generates a location tuple (l_1, l_2) with l_i = (x_i, y_i)
        such that the images put at those locations do not overlap'''
        imsize = np.array(imsize)
        if type(possible_area) == float:
            possible_area = possible_area * imsize
        else:
            possible_area = np.array(possible_area)
        delta = (imsize - np.array(possible_area)) / 2.

        # first round: random tuple
        max_x, max_y = np.array(possible_area) - self.patch_size + delta
        min_x, min_y = delta

        if numObjs == 1:
            # Location drawn from beta-distribution, that broadens with
            # difficulty
            if difficulty == 0:
                # Place exactly at center
                x1 = (max_x + min_x) / 2
                y1 = (max_y + min_y) / 2
            else:
                # Place Beta distributed
                a = self.alpha(difficulty)
                x1, y1 = np.random.beta(a=a, b=a, size=[2])
                # Scale to desired range
                x1 = x1 * (max_x - min_x) + min_x
                y1 = y1 * (max_y - min_y) + min_y

            locations = [[x1, y1]]
            return np.array(locations, dtype=int)

        elif numObjs > 1:
            # Initial location drawn from uniform distribution
            x1 = np.random.randint(min_x, max_x)
            y1 = np.random.randint(min_y, max_y)

            locations = [[x1, y1]]

        for i in range(numObjs - 1):
            redraw = True
            while redraw:
                m = self.steepnessFromDifficulty(difficulty)

                def line(x, x1, y1, m):
                    offset = y1 - x1*m
                    return m*x + offset

                def draw_y(y1, max_y=max_y):
                    n_iter = 0
                    valid = True
                    y2 = np.random.randint(min_y, max_y)
                    while y2 <= y1+self.patch_size and y2 > y1-self.patch_size:
                        if n_iter >= 10:
                            print('Had to redraw')
                            valid = False
                            break
                        y2 = np.random.randint(min_x, max_y)
                        n_iter += 1
                    return y2, valid

                accept = False
                while not accept:
                    y2, valid = draw_y(y1)
                    if not valid:
                        break
                    x_stop1 = line(y2, y1, x1, -m)
                    x_stop2 = line(y2, y1, x1, m)
                    x_min, x_max = np.sort([x_stop1, x_stop2])

                    x_min = np.ceil(x_min)
                    x_min = np.max([0, x_min])
                    x_max = np.floor(x_max)
                    x_max = np.min([max_x, x_max])

                    if x_min == x_max:
                        continue
                    x2 = np.random.randint(x_min, x_max)
                    accept = True
                    redraw = False

            locations.append([x2, y2])

            if np.sqrt((x1-x2)**2 + (y1-y2)**2) < 28:
                # Debug output - This should never occur
                print(x1, y1)
                print(x2, y2)
                print('\n')

        return np.array(locations, dtype=int)

    def sortIndexTuple(self, location_tuple, index_tuple):
        if self.sort_by is None:
            return index_tuple

        idx = 0 if self.sort_by == 'x' else 1
        l_slice = location_tuple[:, idx]

        indeces = l_slice.argsort()
        index_tuple = np.array(index_tuple)[indeces]

        return index_tuple

    def steepnessFromDifficulty(self, difficulty):
        return self.m_0 * 10 ** (2 * difficulty)

    def alpha(self, difficulty):
        d_range = self.max_difficulty
        d = difficulty
        a = self.a_0 * ((1 + float(d_range - d)) / float(d_range))**d

        return a


class CelebsGenerator(ValidationGenerator):

    def __init__(self, mode, imsize=[300, 300], scale_down=True):
        super(CelebsGenerator, self).__init__(imsize)
        self.mode = mode

        self.root = '/home/johannes/Documents/Uni HD/' \
                    + 'Masterarbeit/datasets/CelebsA/'

        self.images_path = os.path.join(self.root, 'img_celeba')
        suffix = '_subset_31_on_off.npy'
        self.labels_path = os.path.join(self.root, '{}'+suffix)
        self.labels = np.load(self.labels_path.format('labels'), 'r')
        self.forbidden_indeces = []  # \
        # np.load(self.labels_path.format('forbidden_images'), 'r')
        self.num_im = self.labels.shape[0]
        self.lr_shape = [25, 25]
        self.imsize = imsize
        self.scale_down = scale_down
        self.l_size = [1, 1]

        self.save_validation_set()
        self.set_mode(mode)

    def image_name(self, index):
        return '{:06}.jpg'.format(index)

    def load_image(self, index):
        name = self.image_name(index)
        name = os.path.join(self.images_path, name)
        image = imread(name)
        if self.scale_down:
            image = imresize(image, self.imsize)
        return image

    def translate_label(self, label):
        # label = np.array(np.where(label > 0)).T
        return label

    def next_train(self):
        index = np.random.randint(low=1, high=self.num_im)
        while index in self.forbidden_indeces:
            print('Drew forbidden index, redrawing')
            index = np.random.randint(low=1, high=self.num_im)
        image = self.load_image(index)
        label = self.labels[index-1]
        label = self.translate_label(label)
        low_res = imresize(image, self.lr_shape)

        return {'features': image, 'low_res': low_res, 'labels': label}


def get_generator_fn(Config, mode='train'):
    '''Sets up the DataGenerator as generator function according to the Config.

    Arguments:
        Config: DRAM Config

    Returns:
        generator_fn: Generator that yields an image, label pair
    '''
    if Config is not None:
        num_objs = Config.getParameter('HyperParameters/maxObjs')
        difficulty = Config.getParameter('Dataset/difficulty')
    else:
        print("+++++++++++++++++++++++++++++++++++++++++++++++")
        print("+  Warning: using generator_fn without config +")
        print("+++++++++++++++++++++++++++++++++++++++++++++++")
        num_objs = 1
        difficulty = 5

    dg = ScatterGenerator('mnist', num_objs, difficulty, mode=mode)

    def generator_fn():
        '''A Wrapper to make the DataGenerator work with the estimator
        feeding_functions library, that only recognises function types.'''
        while True:
            yield next(dg)

    return generator_fn
