import numpy as np
from scipy.misc import imresize
import torch
from torch.autograd import Variable
import warnings


def roi_pool(features, roi, out_size=[11, 11]):
    n_ch, fh, fw = features.size()
    H, W = out_size
    hstart_init, hstopp_init, wstart_init, wstopp_init = roi
    h = hstopp_init - hstart_init
    w = wstopp_init - wstart_init

    bin_size_h = float(h) / float(H)
    bin_size_w = float(w) / float(W)

    out = torch.zeros([n_ch, H, W])
    # print('o', out.size())
    for ph in range(H):
        hstart = int(np.floor(hstart_init + ph*bin_size_h))
        hstopp = int(np.ceil(hstart_init + (ph+1)*bin_size_h))
        for pw in range(W):
            wstart = int(np.floor(wstart_init + pw*bin_size_w))
            wstopp = int(np.ceil(wstart_init + (pw+1)*bin_size_w))

            window = features[:, hstart:hstopp, wstart:wstopp]
            max_pool = torch.max(torch.max(window, 2)[0], 1)[0]
            max_pool = max_pool.view(-1)

            out[:, ph, pw] = max_pool

    return out


def extract_glimpse_np(image, x, y, ps, ref_size, imsize, mode=None):
    warnings.warn(DeprecationWarning('This function has be reimplemented as '
                                     'extract glimpse from tensor'))
    ps = max(1, ps)

    x += imsize[0]  # [0, 200]
    y += imsize[1]

    x /= 2.  # [0, 100]
    y /= 2.

    x -= ps / 2.
    y -= ps / 2.

    start_x, stop_x = int(x), int(x) + int(ps)
    start_y, stop_y = int(y), int(y) + int(ps)

    overshoot_x = max(max(stop_x - imsize[0] - 1, -start_x), 0)
    overshoot_y = max(max(stop_y - imsize[1] - 1, -start_y), 0)

    start_x, stop_x = start_x + overshoot_x, stop_x + overshoot_x
    start_y, stop_y = start_y + overshoot_y, stop_y + overshoot_y

    pad_width = [[overshoot_x, overshoot_x+1],
                 [overshoot_y, overshoot_y+1],
                 [0, 0]]
    image_pad = np.pad(image, pad_width, 'constant')

    patch = image_pad[start_x:stop_x, start_y:stop_y, :]

    if 0 in patch.shape or patch.shape[0] != patch.shape[1]:
        print('Observing:')
        print('x, y, ps:', x, y, ps)
        print('patch shape:', patch.shape)
        print('start and stop x:', start_x, stop_x)
        print('start and stop y:', start_y, stop_y)
        print('padded image shape:', image.shape)
    if patch.shape[-1] == 1:
        patch = imresize(patch[..., 0], [11, 11])
        patch = np.expand_dims(patch, -1)
    else:
        patch = imresize(patch, [11, 11])

    return patch


def extract_glimpse_from_tensor(features, y, x, ps, ref_size, imsize,
                                mode='bilinear'):
    ps = ps.clamp(min=1)
    features = features[0]  # Get rid of batch size of 1
    f_ch, fsize_x, fsize_y = features.size()
    fscale_x = float(fsize_x) / float(imsize[0])
    fscale_y = float(fsize_y) / float(imsize[1])

    # Un center Coordinates given in image pixel space
    x += imsize[0]  # [0, 200]
    y += imsize[1]

    x /= 2.  # [0, 100]
    y /= 2.

    # Get top left corner
    x -= ps / 2.
    y -= ps / 2.

    # Scale to feature size
    x *= fscale_x
    y *= fscale_y
    ps *= fscale_x

    # Floor to get true pixels (can be negative)
    start_x_ = int(x.cpu().numpy())
    stopp_x_ = int((x + ps).cpu().numpy())
    start_y_ = int(y.cpu().numpy())
    stopp_y_ = int((y + ps).cpu().numpy())

    # Clampt to get slice of image
    start_x = np.clip(start_x_, 0, fsize_x)
    stopp_x = np.clip(stopp_x_, 0, fsize_x)
    start_y = np.clip(start_y_, 0, fsize_y)
    stopp_y = np.clip(stopp_y_, 0, fsize_y)

    dx_st = start_x - start_x_
    dx_sp = stopp_x - start_x_
    dy_st = start_y - start_y_
    dy_sp = stopp_y - start_y_

    fx_st = dx_st / max(1, (stopp_x_ - start_x_))
    fx_sp = dx_sp / max(1, (stopp_x_ - start_x_))
    fy_st = dy_st / max(1, (stopp_y_ - start_y_))
    fy_sp = dy_sp / max(1, (stopp_y_ - start_y_))

    gx_st = int(ref_size * fx_st)
    gx_sp = int(ref_size * fx_sp)
    gy_st = int(ref_size * fy_st)
    gy_sp = int(ref_size * fy_sp)

    num_x = gx_sp - gx_st
    num_y = gy_sp - gy_st

    # Empty patch. Could also be filled with noise
    patch = torch.zeros(1, ref_size, ref_size, f_ch)

    if num_x > 0 and num_y > 0:
        if mode == 'nearest':
            raise NotImplementedError('There is a bug in the nearest method. '
                                      'Use roi instead.')
            indeces_x = np.linspace(start_x, stopp_x, num=num_x).astype(int)
            indeces_y = np.linspace(start_y, stopp_y, num=num_y).astype(int)

            YY, XX = np.meshgrid(indeces_y, indeces_x)
            YY = torch.from_numpy(YY).long().cuda()
            XX = torch.from_numpy(XX).long().cuda()
            extracted_glimpse = features.permute(2, 1, 0)[XX, YY]
            extracted_glimpse = extracted_glimpse.unsqueeze(0)
        elif mode == 'bilinear':
            extracted_glimpse = features.permute(2, 1, 0)[start_x:stopp_x,
                                                          start_y:stopp_y]
            extracted_glimpse = extracted_glimpse.permute(2, 0, 1).unsqueeze(0)
            extracted_glimpse = Variable(extracted_glimpse)
            size = [num_x, num_y]
            extracted_glimpse = torch.nn.functional.upsample(extracted_glimpse,
                                                             size=size,
                                                             mode=mode)
            extracted_glimpse = extracted_glimpse.permute(0, 2, 3, 1).data
        elif mode == 'roi':
            roi = [float(start_x), float(stopp_x),
                   float(start_y), float(stopp_y)]
            size = [num_x, num_y]
            extracted_glimpse = roi_pool(features.permute(0, 2, 1), roi, size)
            extracted_glimpse = extracted_glimpse.permute(1, 2, 0)
        else:
            raise ValueError('Unknown extraction method {}. Can be one of '
                             'nearest, bilinear, roi.')

        patch[:, gx_st:gx_sp, gy_st:gy_sp, :] = extracted_glimpse
    patch = patch.permute(0, 3, 2, 1)

    return patch


extract_glimpse = extract_glimpse_from_tensor
