import torch
from torch.autograd import Variable

from .util import extract_glimpse


class GlimpseEnv(object):
    metadata = {'render.modes': ['human']}

    def __init__(self,
                 generator,
                 ref_size=11,
                 extract_mode='roi',
                 sorted_targets=True,
                 fss=True,
                 pred_thresh=10.,
                 eos_thresh=20.,
                 use_cuda=True,
                 testmode=False):
        '''Setup the environment given the number of objects that are supposed
        to be placed and the difficulty with wich that should happen.

        Arguments:
            generator: Iterator, returns dict with keys 'features', 'low_res',
                'targets'.
            ref_size: width and heigth of observed patch
            extract_mode: interpolation method to use, when generating
                glimpses. Can be 'nearest' or 'biliniear'.
            sorted_targets: When predicting, return the first target from the
                set of remaining targets. Else the reward is determined based
                on the fact if the prediction is in the set of all targets.
                Default: True
            fss: Fixed step size, if True ignore all signals. Default: True
            pred_thresh, eos_thresh: Thresholds to determin prediction and
                end of sequence action. Defaults: 10, 20
            use_cuda: Bool, used to determine if to push images to gpu or not.
            testmode: In testmode the stop criterion is again to predict all
                labels.
        '''

        self.pred_thresh = pred_thresh
        self.eos_thresh = eos_thresh
        self.ref_size = ref_size
        self.extract_mode = extract_mode
        self.sorted_targets = sorted_targets
        self.fixed_step_size = fss is not None
        if type(fss) is int:
            self.step_size = fss
        elif fss is not None:
            self.step_size = 3
        self.generator = generator
        self.N_classes = generator.num_classes
        self.use_cuda = use_cuda
        self.testmode = testmode

    def step(self, action):
        observation, self.image_current = self.observe(action)
        reward, proposed_target = self.reward(action)
        episode_over = self.check_terminal(action)
        info = {'target': proposed_target}

        self.ts += 1

        return observation, reward, episode_over, info

    def initial_step(self, action):
        '''Step without prediction -> No prediction possible hence no penality
        for "wrong" prediction.'''
        observation = self.observe(action)
        self.ts += 1

        return observation, 0., False, {}

    def reset(self):
        self.world = next(self.generator)

        self.image = self.world['features']
        if 'image' in self.world:
            self.image_orig = self.world['image']
        else:
            self.image_orig = self.image
        self.imsize = list(self.image_orig[0].permute(1, 2, 0).size())
        self.image_sequence = False
        if len(self.image.size()) == 5:
            self.image_sequence = True
        self.low_res = self.world['low_res']

        self.targets = self.world['labels'].squeeze().cpu().numpy().tolist()
        if not isinstance(self.targets, list):
            self.targets = [self.targets]
        self.all_targets = list(self.targets)
        self.N_obj = len(self.targets)

        self.N_pred = 0
        self.ts = -1

        if self.use_cuda:
            self.image = self.image.cuda()
            self.low_res = self.low_res.cuda()

        return self.low_res, self.imsize

    def restart(self):
        '''Reexperience episode -> Reset prediction list'''
        self.N_pred = 0
        self.ts = -1
        self.targets = list(self.all_targets)

    def hard_restart(self):
        '''Also resets valid counter in data generator.'''
        self.reset()
        self.generator.reset_valid_counter()

    def _render(self, mode='human', close=False):
        return 0

    def observe(self, action):
        x, y = action['location']  # [-100, 100]
        ps = action['patch_size']
        if self.image_sequence:
            image = self.image[self.ts]
        else:
            image = self.image

        x = x.data.clone()
        y = y.data.clone()
        ps = ps.data.clone()
        patch = extract_glimpse(image, x, y, ps, self.ref_size, self.imsize,
                                self.extract_mode)

        if self.use_cuda:
            patch = patch.cuda()
        return patch, image

    def reward(self, action):
        # Translate Prediction to class/label int
        pred = action['prediction']
        if pred is not None:
            val, pred = pred.data.max(-1)
            pred = int(pred.cpu().numpy())
        pred_signal = action['pred_signal']

        # Prediction can be made at a fixed time interval or if the agent says
        # so
        if not self.fixed_step_size:
            pred_cond = pred_signal > self.pred_thresh
        elif self.step_size == 1:
            pred_cond = self.ts >= 0
        else:
            pred_cond = (self.ts + 1) % self.step_size == 0 and self.ts > 0

        # If a prediction is made reward accordingly else penalize time step
        if pred_cond:
            # Sorted targets => target corresponds to the number of prediction
            if self.sorted_targets:
                target = self.targets[self.N_pred]
                correct_prediction = pred == target
            # Unsorted targets => check if prediction is in the set of
            # remaining labels.
            else:
                target = -1
                correct_prediction = False
                if pred in self.targets:
                    correct_prediction = True
                    self.targets.remove(pred)

            R = 2. if correct_prediction else -4.
            self.N_pred += 1
        else:
            R = -1.
            target = None
        self.last_reward = R
        return R, target

    def get_targets(self):
        targets = torch.zeros(1, self.N_classes)
        for i in self.all_targets:
            targets[0, i] = 1
        targets = Variable(targets)

        if self.use_cuda:
            targets = targets.cuda()

        return targets

    def propose_target(self, prediction):
        '''For now: The order in which targets are given matters and they are
        simply output on by another.'''

        return self.targets[0]

    def check_terminal(self, action):
        '''For now: Terminal is reached, when the number of predictions matches
        the number of objects in the scene. Later this is supposed to be
        handled by an eos signal.'''

        if self.N_pred == self.N_obj:
            return True
        elif self.last_reward == -4 and self.sorted_targets \
                and not self.testmode:
            return True
        return False
