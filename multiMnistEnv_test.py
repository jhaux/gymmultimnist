from gym_multiMnist.envs.multiMnist import MultiMnistEnv
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


if __name__ == '__main__':
    actions = [
        {'location': [-90, 90],
         'patch_size': 20,
         'pred_signal': 0.5,
         'eos_signal': 0.5,
         'prediction': 1},
        {'location': [-90, -90],
         'patch_size': 40,
         'pred_signal': 0.5,
         'eos_signal': 0.5,
         'prediction': 1},
        {'location': [20, -10],
         'patch_size': 40,
         'pred_signal': 2.5,
         'eos_signal': 1.5,
         'prediction': 0},
        {'location': [0, 0],
         'patch_size': 30,
         'pred_signal': 1.5,
         'eos_signal': 2.5,
         'prediction': 1}
        ]

    env = MultiMnistEnv(1, 0)
    goal_r = 1
    while True:
        env.reset()
        obs = []
        rs = []
        terms = []
        for a in actions:
            o, r, term, _ = env.step(a)
            print r
            obs.append(o)
            rs.append(r)
            terms.append(term)
            if term:
                env.reset()
        if r == goal_r and goal_r == 1:
            N_act = len(actions)
            f = plt.figure()
            gs = gridspec.GridSpec(1, N_act)
            for i in range(N_act):
                ax = f.add_subplot(gs[i])
                ax.imshow(obs[i][:, :, 0])
                p = actions[i]['pred_signal'] > 1
                e = actions[i]['eos_signal'] > 2
                ax.set_title('r={}, t={}, p={}, e={}'.format(rs[i], terms[i],
                                                             p, e))
                ax.axis('off')

            plt.show()
            goal_r += 0

            break
